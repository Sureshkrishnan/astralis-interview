$(document).ready(function () {
    $('.administration a').addClass('toggled')
    $('.administration .ml-menu').css({ 'display': 'block' })
    loadPage();
});

var API_URL = "http://localhost:8086/astralis" 

function loadPage(){
    $('#encryption').val('')
    $("#encrypt_alert").html(" ");
}


function encryption(){
    var encryptText = $('#encryption').val()
    if (!encryptText) {
        $('#encryption').css("border-color", "red");
        $("#encrypt_alert").html("Please enter text").css('color', 'red')
        $('#encryption').keyup(function (e) {
            if ($(this).val() != "") {
                $('#encryption').css("border-color", "#e9ecef");
                $("#encrypt_alert").html("");
            }

        })
    }else{
        var reqObj = {
            "text": encryptText
        }

        $.ajax({
            url: API_URL + "/cipher/encrypt",
            contentType: "application/json",
            type: "POST",
            data: JSON.stringify(reqObj),
            success: function (response) {
                if (response.status === true) {
                    $("#encrypt_alert").html("<span style='color: #00000'>Encrypted text is <span style='color: green'>" + response.result + "</span></span>")
                } else {
                    swal({
                        title: "Error",
                        text: response.message,
                        type: "error",
                    })
                }
            },
            error: function () {
                swal(
                    'Something went wrong! Please try again later.'
                )
            }
        })
    }
}


function decryption(){
    var decryptText = $('#decryption').val()
    if (!decryptText) {
        $('#decryption').css("border-color", "red");
        $("#decrypt_alert").html("Please enter text").css('color', 'red')
        $('#decryption').keyup(function (e) {
            if ($(this).val() != "") {
                $('#decryption').css("border-color", "#e9ecef");
                $("#decrypt_alert").html("");
            }

        })
    }else{
        var reqObj = {
            "text": decryptText
        }

        $.ajax({
            url: API_URL + "/cipher/decrypt",
            contentType: "application/json",
            type: "POST",
            data: JSON.stringify(reqObj),
            success: function (response) {
                if (response.status === true) {
                    $("#decrypt_alert").html("<span style='color: #00000'>Plain text is <span style='color: green'>" + response.result + "</span></span>")
                } else {
                    swal({
                        title: "Error",
                        text: response.message,
                        type: "error",
                    })
                }
            },
            error: function () {
                swal(
                    'Something went wrong! Please try again later.'
                )
            }
        })
    }
}