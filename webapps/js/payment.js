var paymentTable = null;
var record_list;
var paymentId = null;
var paymentObj = {}
var API_URL = "http://localhost:8086/astralis"
var company_id = $("#COMPANY_ID").val()
var DATE_TIME_FORMAT = "MM-DD-YYYY HH:mm A"
$(document).ready(function () {
    $('.administration a').addClass('toggled')
    $('.administration .ml-menu').css({ 'display': 'block' })
    loadPage();
});



function loadPage() {

    if (paymentTable) {
        paymentTable.destroy();
        $("#paymentTable").html("");
    }

    var fields = [

        {
            mData: 'name',
            sTitle: 'Name',
            sWidth: '25%',
            orderable: false,
            mRender: function (data, type, row) {
                return data ? data : "-";
            }
        },
        {
            mData: 'acc_num',
            sTitle: 'Account Number',
            sWidth: '25%',
            orderable: false,
            mRender: function (data, type, row) {
                return data ? data : "-";
            }
        },
        {
            mData: 'ifsc',
            sTitle: 'IFSC',
            sWidth: '25%',
            orderable: false,
            mRender: function (data, type, row) {
                return data ? data : "-";
            }
        },
        {
            mData: 'amount',
            sTitle: 'Amount',
            sWidth: '25%',
            orderable: false,
            mRender: function (data, type, row) {
                return data ? data : "-";
            }
        },
        {
            mData: 'create_ts',
            sTitle: 'Created Time',
            sWidth: '30%',
            orderable: false,
            mRender: function (data, type, row) {
                return moment(data).format(DATE_TIME_FORMAT);
            }
        },
        {
            mData: 'update_ts',
            sTitle: 'Updated Time',
            mRender: function (data, type, row) {
                return moment(data).format(DATE_TIME_FORMAT);
            }
        },
        {
            mData: 'action',
            sTitle: 'Action',
            orderable: false,
            sWidth: '5%',
            mRender: function (data, type, row) {
                var str = '<button class="btn-default" data-toggle="tooltip" title="Edit department" onclick="openModal(2,\'' + row['_id'] + '\')"><i class="fa fa-pencil" aria-hidden="true"></i></button>\n' +
                    '<button class="btn-default" data-toggle="tooltip" title="Delete department" onclick="openModal(3,\'' + row['_id'] + '\')"><i class="fa fa-trash" aria-hidden="true"></i></button>'
                return str;
            }
        },

    ];

    var tableOption = {
        fixedHeader: true,
        responsive: false,
        paging: true,
        searching: false,
        aaSorting: [[5, 'desc']],
        "ordering": true,
        iDisplayLength: 10,
        lengthMenu: [[10, 50, 100], [10, 50, 100]],
        aoColumns: fields,
        language: {
            paginate: {
                previous: "Previous",
                next: "Next",
            },
            lengthMenu: "Show" + " _MENU_ " + "records",
            loadingRecords: '<i class="fa fa-spinner fa-spin"></i>&nbsp;' + "Loading...",
            processing: '<i class="fa fa-spinner fa-spin"></i>&nbsp' + "Processing...",
            searchPlaceholder: "Department",
            search: "Search :",
            emptyTable: "No data available",
            info: "Showing" + " _START_ to _END_ of _TOTAL_ " + "record(s)",
            infoEmpty: "Showing" + " 0 to 0 of 0 " + "record(s)",
        },
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": API_URL + "/payment/list",
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {

            var queryParams = {
                "limit": oSettings._iDisplayLength,
                "skip": oSettings._iDisplayStart
            }
            var searchText = oSettings.oPreviousSearch.sSearch;


            oSettings.jqXHR = $.ajax({
                "dataType": 'json',
                "contentType": 'application/json',
                "type": "POST",
                "url": sSource,
                "data": JSON.stringify(queryParams),
                success: function (data) {
                    var resultData = data.result.data;
                    var totalCount = data.result.total ? data.result.total : 0
                    $(".totalCount").html(totalCount);
                    record_list = data.result.data;
                    resultData['draw'] = oSettings.iDraw;
                    fnCallback(resultData);
                },
                error: function (e) {
                    swal({
                        title: "Error",
                        text: "Something went wrong! Please try again later.",
                        type: "error",
                    })
                }
            });
        }

    };

    paymentTable = $("#paymentTable").DataTable(tableOption);

}


function openModal(type, id, department) {
    if (type === 1) {
        $('#paymentModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $("#paymentModal form")[0].reset();
        $('#payment_add_butt').removeClass('d-none')
        $('#payment_update_butt').addClass('d-none')
        $("#paymentModal .modal-title").html('Add Payment');
    } else if (type === 2) {

        $('#payment_add_butt').addClass('d-none')
        $('#payment_update_butt').removeClass('d-none')

        $('#paymentModal').modal({
            backdrop: 'static',
            keyboard: false
        })

        $("#paymentModal form")[0].reset();
        $("#paymentModal .modal-title").html('Update Payment');
        for (var i = 0; i < record_list.data.length; i++) {
            if (id === record_list.data[i]._id) {
                paymentId = id;
                paymentObj = record_list.data[i];

            }
        }
        $('#name').val(paymentObj.name);
        $('#acc_num').val(paymentObj.acc_num);
        $('#ifsc').val(paymentObj.ifsc);
        $('#amount').val(paymentObj.amount);


    } else if (type == 3) {
        for (var i = 0; i < record_list.data.length; i++) {
            if (id === record_list.data[i]._id) {
                paymentId = id;
                paymentObj = record_list.data[i];

            }
        }
        deleteDepartment(paymentId);
    }
}

function proceedPayment(action) {

    var name = $('#name').val().trim();
    var acc_num = $('#acc_num').val().trim();
    var ifsc = $('#ifsc').val().trim();
    var amount = $('#amount').val().trim();
    const regExp = "^[A-Z]{4}0[A-Z0-9]{6}$"

    if (!name) {
        $('#name').css("border-color", "red");
        $("#name_alert").html("Name is required").css('color', 'red')
        $('#name').keyup(function (e) {
            if ($(this).val() != "") {
                $('#name').css("border-color", "#e9ecef");
                $("#name_alert").html(" ");
            }

        })
    }
    else if (!acc_num) {
        $('#acc_num').css("border-color", "red");
        $("#acc_alert").html("Account Number is required").css('color', 'red')
        $('#acc_num').keyup(function (e) {
            if ($(this).val() != "") {
                $('#acc_num').css("border-color", "#e9ecef");
                $("#acc_alert").html(" ");
            }

        })

    }
    else if (acc_num.length != 16) {
        $('#acc_num').css("border-color", "red");
        $("#acc_alert").html("please enter valid 16 digit account number").css('color', 'red')
        $('#acc_num').keyup(function (e) {
            if ($(this).val() != "") {
                $('#acc_num').css("border-color", "#e9ecef");
                $("#acc_alert").html(" ");
            }

        })

    }
    else if (!ifsc) {
        $('#ifsc').css("border-color", "red");
        $("#ifsc_alert").html("IFSC is required").css('color', 'red')
        $('#ifsc').keyup(function (e) {
            if ($(this).val() != "") {
                $('#ifsc').css("border-color", "#e9ecef");
                $("#ifsc_alert").html(" ");
            }

        })

    }
    else if (!ifsc.match(regExp)) {
        $('#ifsc').css("border-color", "red");
        $("#ifsc_alert").html("Please enter valid IFSC code").css('color', 'red')
        $('#ifsc').keyup(function (e) {
            if ($(this).val() != "") {
                $('#ifsc').css("border-color", "#e9ecef");
                $("#ifsc_alert").html(" ");
            }

        })

    }
    else if (!amount) {
        $('#amount').css("border-color", "red");
        $("#amount_alert").html("Amount is required").css('color', 'red')
        $('#amount').keyup(function (e) {
            if ($(this).val() != "") {
                $('#amount').css("border-color", "#e9ecef");
                $("#amount_alert").html(" ");
            }

        })

    }
    else if (amount <= 150) {
        $('#amount').css("border-color", "red");
        $("#amount_alert").html("Minimum amount is 150").css('color', 'red')
        $('#amount').keyup(function (e) {
            if ($(this).val() != "") {
                $('#amount').css("border-color", "#e9ecef");
                $("#amount_alert").html(" ");
            }

        })

    }
    else if (amount >= 1500) {
        $('#amount').css("border-color", "red");
        $("#amount_alert").html("maximum amount is 1500").css('color', 'red')
        $('#amount').keyup(function (e) {
            if ($(this).val() != "") {
                $('#amount').css("border-color", "#e9ecef");
                $("#amount_alert").html(" ");
            }

        })

    }
    else {
        var url = ''
        if (action === 'add') {
            url = API_URL + "/payment/add"
            $('#payment_add_butt').attr('disabled', true).html("<i class='fa fa-spinner fa-spin'></i> " + 'Adding')
        } else {
            url = API_URL + "/payment/update"
            $('#payment_update_butt').attr('disabled', true).html("<i class='fa fa-spinner fa-spin'></i> " + 'Updating')
        }
        var requestObj = {
            name: name,
            acc_num: acc_num,
            ifsc: ifsc,
            amount: amount,
            update_ts: new Date().getTime(),
            create_ts: new Date().getTime()
        }

        if (action === 'update') {
            requestObj['_id'] = paymentId
        }

        $.ajax({
            url: url,
            contentType: "application/json",
            type: "POST",
            data: JSON.stringify(requestObj),
            success: function (response) {
                if (response.status === true) {
                    $("#paymentModal").modal('hide');
                    if (action === 'add') {
                        $('#payment_add_butt').removeAttr('disabled').html('Add')
                    } else {
                        $('#payment_update_butt').removeAttr('disabled').html('update')
                    }
                    setTimeout(function () {
                        loadPage()
                    }, 1000)
                    swal({
                        title: "Success",
                        text: response.message,
                        type: "success",
                    })
                } else {
                    $("#paymentModal").modal('hide');
                    if (action === 'add') {
                        $('#payment_add_butt').removeAttr('disabled').html('Add')
                    } else {
                        $('#payment_update_butt').removeAttr('disabled').html('update')
                    }
                    swal({
                        title: "Error",
                        text: response.message,
                        type: "error",
                    })
                }
            },
            error: function () {
                if (action === 'add') {
                    $('#payment_add_butt').removeAttr('disabled').html('Add')
                } else {
                    $('#payment_update_butt').removeAttr('disabled').html('update')
                }

                $("#paymentModal").modal('hide');
                swal(
                    'Something went wrong! Please try again later.'
                )
            }
        })

    }
}

function deleteDepartment(deptId) {
    swal({
        title: "Are you sure?",
        text: "Do you want to delete ",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Ok',
        cancelButtonText: 'Cancel',
        confirmButtonColor: '#ce362c'
    })
        .then((willDelete) => {
            if (willDelete.value) {
                var deleteObj = {
                    _id: deptId
                };
                $.ajax({
                    url: API_URL + "/payment/delete",
                    data: JSON.stringify(deleteObj),
                    contentType: "application/json",
                    type: 'POST',
                    success: function (response) {
                        if (response.status) {
                            $("#paymentModal").modal('hide');

                            setTimeout(function () {
                                loadPage()
                            }, 1000)
                            swal({
                                title: "Success",
                                text: response.message,
                                type: "success",
                            })
                        } else {
                            swal({
                                title: "Error",
                                text: response.message,
                                type: "error",
                            })
                        }

                    },
                    error: function (e) {
                        swal({
                            title: "Error",
                            text: "Something went wrong! Please try again later.",
                            type: "error",
                        })
                    }
                })
            } else {

            }
        });
}



//Form Validation is text
function isAlfa(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 34 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
        return false;
    }
    return true;
}

//Form Validation is Number
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
