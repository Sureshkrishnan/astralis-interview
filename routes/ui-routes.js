

var UIRoutes = function (app, router) {
    this.app = app
    this.router = router
    this.init()
}

module.exports = UIRoutes

UIRoutes.prototype.init = function (req, res, next) {

    const self = this

    self.router.get("/", function (req, res) {
        res.render('payment.html')
    })

    self.router.get("/cipher", function (req, res) {
        res.render('cipher.html',{
            layout: 'layout.html'
        })
    })

    self.app.use("/astralis", self.router)
}