var Payment = require("../modules/payment")
var Cipher = require("../modules/cipher")

var APIRoutes = function (app, router) {
    this.app = app
    this.router = router
    this.payment = new Payment(app, router)
    this.cipher = new Cipher(app, router)
    this.init()
}

module.exports = APIRoutes

APIRoutes.prototype.init = function (req, res, next) {

    const self = this

    self.router.post("/payment/:action", function (req, res) {
        self.payment.paymentActions(req, res)
    })

    self.router.post("/cipher/:action", function (req, res) {
        self.cipher.cipherActions(req, res)
    })

    self.app.use("/astralis", self.router)
}