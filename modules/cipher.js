
var CryptoJS = require("crypto-js")

var Cipher = function (app, router) {
    this.app = app;
    this.router = router
}

module.exports = Cipher


Cipher.prototype.cipherActions = function (req, res) {

    const self = this

    var action = req['params']['action']

    if (action === 'encrypt') {
        self.encryptText(req, res)
    } else if (action === 'decrypt') {
        self.decryptText(req, res)
    }

}


Cipher.prototype.encryptText = function (req, res) {

    var plaintext = req.body.text

    res.status(200).json({ status: true, result: CryptoJS.AES.encrypt(plaintext, process.env.SECRECT_KEY).toString(), message: "Text encrypted successfully" })

}


Cipher.prototype.decryptText = function (req, res) {

    var plaintext = req.body.text

    var bytes  = CryptoJS.AES.decrypt(plaintext, process.env.SECRECT_KEY);
    var originalText = bytes.toString(CryptoJS.enc.Utf8)

    res.status(200).json({ status: true, result: originalText, message: "Text decrypted successfully" })

}





