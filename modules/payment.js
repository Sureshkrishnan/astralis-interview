var { paymentSchemaDetails } = require("../models/models")

var Payment = function (app, router) {
    this.app = app;
    this.router = router
}

module.exports = Payment


Payment.prototype.paymentActions = function (req, res) {

    const self = this

    var action = req['params']['action']

    if (action === 'add') {
        self.addPaymentDetails(req, res)
    } else if (action === 'list') {
        self.listPaymentDetails(req, res)
    } else if (action === 'update') {
        self.updatePaymentDetails(req, res)
    } else if (action === 'delete') {
        self.deletePaymentDetails(req, res)
    }

}


Payment.prototype.addPaymentDetails = function (req, res) {

    const self = this

    var reqObj = req.body

    paymentSchemaDetails.insertMany(reqObj, function (err, result) {
        if (!err) {
            res.status(200).json({ status: true, result: result._id, message: "Payment details added successfully" })
        } else {
            res.status(500).json({ status: false, result: err, message: "Error in adding payment details" })
        }
    })

}

Payment.prototype.listPaymentDetails = function (req, res) {

    const self = this

    var reqObj = req.body

    var limit = Number(reqObj.limit)

    var skip = Number(reqObj.skip)

    paymentSchemaDetails.find({}, function (err, result) {
        if (!err) {
            paymentSchemaDetails.countDocuments(function (err, count) {
                if (!err) {
                    var resultObj = {
                        "total": count,
                        "data": {
                            "recordsTotal": count,
                            "recordsFiltered": count,
                            "data": result
                        }
                    }
                    res.status(200).json({ status: true, result: resultObj, message: "Payment details fetched successfully" })
                } else {
                    res.status(500).json({ status: false, result: err, message: "Error in fetching Payment details" })
                }
            })

        } else {
            res.status(500).json({ status: false, result: err, message: "Error in fetching Payment details" })
        }
    }).limit(limit).skip(skip)

}


Payment.prototype.updatePaymentDetails = function (req, res) {

    const self = this

    var reqObj = req.body

    var id = req.body._id

    delete reqObj['_id']

    paymentSchemaDetails.findByIdAndUpdate(id, reqObj, { "useFindAndModify": false }, function (err, result) {
        if (!err) {
            res.status(200).json({ status: true, result: result._id, message: "Payment details updated successfully" })
        } else {
            res.status(500).json({ status: false, result: err, message: "Error in updating Payment details" })
        }
    })

}


Payment.prototype.deletePaymentDetails = function (req, res) {

    const self = this

    var reqObj = req.body

    var id = req.body._id

    delete reqObj['_id']

    paymentSchemaDetails.findByIdAndDelete(id, { "useFindAndModify": false }, function (err, result) {
        if (!err) {
            res.status(200).json({ status: true, result: result._id, message: "Payment details deleted successfully" })
        } else {
            res.status(500).json({ status: false, result: err, message: "Error in deleting Payment details" })
        }
    })

}