const mongoose = require("mongoose")
mongoose.Promise = global.Promise

const dotenv = require('dotenv').config()

mongoose.connect(process.env.DB, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }, function (err, result) {
  if(!err){
      console.log("DB connected successfully")
  }else{
      console.log("Error in DB connection")
  }
})