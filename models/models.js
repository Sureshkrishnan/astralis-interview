const mongoose = require('mongoose')
mongoose.Promise = global.Promise

const paymentSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    acc_num: {
        type: String,
        required: true
    },
    ifsc: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true,
    }
})


const paymentSchemaDetails = mongoose.model("payment", paymentSchema)


module.exports = { paymentSchemaDetails }