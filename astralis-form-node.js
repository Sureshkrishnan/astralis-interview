// Import Required modules 

const express = require('express')
const bodyParser = require("body-parser")
const app = express()
const router = express.Router()
const layout = require('express-layout');
const uuid = require('uuid-random')
const cors = require("cors")
const conf = require("./conf")
const path = require('path')
const cookieParser = require('cookie-parser')
const expressSession = require('express-session')


app.use(bodyParser.json())
app.use(cors())
app.use(cookieParser("ASTRALIS-FORM"))
app.use(layout());
app.conf = conf

/* For static path */
app.set('views', path.join(__dirname, 'views'));
app.use('/css', express.static(__dirname + '/webapps/css'));
app.use('/js', express.static(__dirname + '/webapps/js'));

app.engine('html',require('ejs').renderFile)
app.set("view engine","html")
app.set("view options", {layout: "layout.html"});

var sessionObj = {
    secret: "ASTRALIS-FORM",
    resave: false,
    saveUninitialized: true,
    cookie: {
        maxAge: 3 * 60 * 60 * 1000,
        secure: false
    }
}

app.use(expressSession(sessionObj))

var server = require('http').Server(app)
server.listen(8086)
console.log("Server listening on port - 8086")


var APIRoutes = require("./routes/api-routes")
new APIRoutes(app, router)

var UIRoutes = require("./routes/ui-routes")
new UIRoutes(app, router)


